// Function to generate a random number between min and max
function getRandomValue(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Function to set the min and max values based on the difficulty level
function setMinMaxValues(difficulty) {
  let base = getRandomValue(0, 255);
  let min;
  let max;

  if (difficulty == 0) {
    min = 0;
    max = 255;
  } else if (difficulty == 1) {
    base = getRandomValue(40, 215);
    min = base - 40;
    max = base + 40;
  } else if (difficulty == 2) {
    base = getRandomValue(20, 235);
    min = base - 20;
    max = base + 20;
  } else {
    base = getRandomValue(5, 250);
    min = base - 5;
    max = base + 5;
  }

  return { base, min, max };
}

// Function to set the RGB values based on the base color
function setRGBValues(baseColour, base, min, max) {
  let red;
  let green;
  let blue;

  if ((baseColour = "Red")) {
    red = base;
    green = getRandomValue(min, max);
    blue = getRandomValue(min, max);
  } else if ((baseColour = "Green")) {
    green = base;
    blue = getRandomValue(min, max);
    red = getRandomValue(min, max);
  } else {
    blue = base;
    red = getRandomValue(min, max);
    green = getRandomValue(min, max);
  }

  return { red, green, blue };
}

// Updated getRandomRGBColor function
function getRandomRGBColor(difficulty) {
  let baseColour = document.getElementById("colours");
  let { base, min, max } = setMinMaxValues(difficulty);
  let { red, green, blue } = setRGBValues(baseColour, base, min, max);
  let rgbColor = `rgb(${red}, ${green}, ${blue})`;
  return rgbColor;
}

// Function to check if the current RGB value has the most r/g/b depending on which colour you're playing
function isColour(rgbValue) {
  // Use regex to extract the numbers from the rgb string
  let rgbNumberString = rgbValue.match(/\d+/g);
  let result = false;

  // If the colourIndex is 0 (Red), check if the red value is the highest
  if (colourIndex == 0) {
    if (
      parseInt(rgbNumberString[0]) > parseInt(rgbNumberString[1]) &&
      parseInt(rgbNumberString[0]) > parseInt(rgbNumberString[2])
    ) {
      result = true;
      // Increment the count of matching tiles
      matchingTiles++;
    }
  }
  // If the colourIndex is 1 (Green), check if the green value is the highest
  else if (colourIndex == 1) {
    if (
      parseInt(rgbNumberString[1]) > parseInt(rgbNumberString[0]) &&
      parseInt(rgbNumberString[1]) > parseInt(rgbNumberString[2])
    ) {
      result = true;
      // Increment the count of matching tiles
      matchingTiles++;
    }
  }
  // If the colourIndex is neither 0 nor 1 (Blue), check if the blue value is the highest
  else {
    if (
      parseInt(rgbNumberString[2]) > parseInt(rgbNumberString[0]) &&
      parseInt(rgbNumberString[2]) > parseInt(rgbNumberString[1])
    ) {
      result = true;
      // Increment the count of matching tiles
      matchingTiles++;
    }
  }

  // Return the result
  return result;
}
