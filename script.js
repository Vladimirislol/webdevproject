"use strict";

// Global variables
let lookingForColour;
let colourIndex;
let matchingTiles = 0;
let selectedCells = 0;
let guessSelected = 0;
let globalBoardSize = 0;
let globalDifficulty = 0;

// Event listener for DOMContentLoaded
document.addEventListener("DOMContentLoaded", setup);

// Setup function to initialize the game
function setup() {
  // Initialize game buttons and their event listeners
  initializeGameButtons();

  // Display high scores
  displayHighScores();
}

// Function to initialize game buttons and their event listeners
function initializeGameButtons() {
  const startGameButton = document.getElementById("startGameButton");
  const submitGuessesButton = document.getElementById("submitGuessesButton");
  const clearHighScoresButton = document.getElementById("resetButton");

  // Set background color for start game button
  setBackgroundColour(startGameButton);

  // Event listener for color change
  document.querySelector("#colours").addEventListener("change", function () {
    setBackgroundColour(startGameButton);
  });

  // Event listener for submit guesses button
  submitGuessesButton.addEventListener("click", submitGuess);

  // Event listener for start game button
  startGameButton.addEventListener("click", startGame);

  // Event listener for clear high scores button
  clearHighScoresButton.addEventListener("click", clearHighScores);
}

// Function to set background color for start game button
function setBackgroundColour(startGameButton) {
  let backgroundColour = document.querySelector("#colours").value;
  if (backgroundColour == "blue") {
    backgroundColour = "lightblue";
  }
  startGameButton.style.background = backgroundColour;
}

// Function to start the game
function startGame(event) {
  event.preventDefault();
  lookingForColour = document.querySelector("#colours").value;
  submitGuessesButton.disabled = false;
  setColourIndex();
  createTable();
  disableGameOptions();
}

// Function to set colour index based on the selected colour
function setColourIndex() {
  if (lookingForColour == "red") {
    colourIndex = 0;
  } else if (lookingForColour == "green") {
    colourIndex = 1;
  } else {
    colourIndex = 2;
  }
}

// Function to disable game options after the game starts
function disableGameOptions() {
  document.getElementById("colours").disabled = true;
  document.getElementById("name").disabled = true;
  document.getElementById("size").disabled = true;
  document.getElementById("difficulty").disabled = true;
}

// Function to generate a random number between min and max
function getRandomValue(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

// Function to set the min and max values based on the difficulty level
function setMinMaxValues(difficulty) {
  let base = getRandomValue(0, 255);
  let min;
  let max;

  if (difficulty == 0) {
    min = 0;
    max = 255;
  } else if (difficulty == 1) {
    base = getRandomValue(40, 215);
    min = base - 40;
    max = base + 40;
  } else if (difficulty == 2) {
    base = getRandomValue(20, 235);
    min = base - 20;
    max = base + 20;
  } else {
    base = getRandomValue(5, 250);
    min = base - 5;
    max = base + 5;
  }

  return { base, min, max };
}

// Function to set the RGB values based on the base color
function setRGBValues(baseColour, base, min, max) {
  let red;
  let green;
  let blue;

  if ((baseColour = "Red")) {
    red = base;
    green = getRandomValue(min, max);
    blue = getRandomValue(min, max);
  } else if ((baseColour = "Green")) {
    green = base;
    blue = getRandomValue(min, max);
    red = getRandomValue(min, max);
  } else {
    blue = base;
    red = getRandomValue(min, max);
    green = getRandomValue(min, max);
  }

  return { red, green, blue };
}

// Updated getRandomRGBColor function
function getRandomRGBColor(difficulty) {
  let baseColour = document.getElementById("colours");
  let { base, min, max } = setMinMaxValues(difficulty);
  let { red, green, blue } = setRGBValues(baseColour, base, min, max);
  let rgbColor = `rgb(${red}, ${green}, ${blue})`;
  return rgbColor;
}

// Function to check if the current RGB value has the most r/g/b depending on which colour you're playing
function isColour(rgbValue) {
  // Use regex to extract the numbers from the rgb string
  let rgbNumberString = rgbValue.match(/\d+/g);
  let result = false;

  // If the colourIndex is 0 (Red), check if the red value is the highest
  if (colourIndex == 0) {
    if (
      parseInt(rgbNumberString[0]) > parseInt(rgbNumberString[1]) &&
      parseInt(rgbNumberString[0]) > parseInt(rgbNumberString[2])
    ) {
      result = true;
      // Increment the count of matching tiles
      matchingTiles++;
    }
  }
  // If the colourIndex is 1 (Green), check if the green value is the highest
  else if (colourIndex == 1) {
    if (
      parseInt(rgbNumberString[1]) > parseInt(rgbNumberString[0]) &&
      parseInt(rgbNumberString[1]) > parseInt(rgbNumberString[2])
    ) {
      result = true;
      // Increment the count of matching tiles
      matchingTiles++;
    }
  }
  // If the colourIndex is neither 0 nor 1 (Blue), check if the blue value is the highest
  else {
    if (
      parseInt(rgbNumberString[2]) > parseInt(rgbNumberString[0]) &&
      parseInt(rgbNumberString[2]) > parseInt(rgbNumberString[1])
    ) {
      result = true;
      // Increment the count of matching tiles
      matchingTiles++;
    }
  }

  // Return the result
  return result;
}

// Function to create a new table
function createNewTable() {
  const table = document.createElement("table");
  table.id = "gameTable";
  return table;
}

// Function to create a new row
function createNewRow() {
  return document.createElement("tr");
}

// Function to create a new cell
function createNewCell(id, color) {
  const cell = document.createElement("td");
  cell.style.color = color;
  cell.style.backgroundColor = color;
  cell.setAttribute("right-colour", isColour(color));
  cell.id = id;
  return cell;
}

// Function to determine the color component with the highest value
function getMaxColorComponent(color) {
  let rgbValues = color.match(/\d+/g);
  const redValue = parseInt(rgbValues[0]);
  const greenValue = parseInt(rgbValues[1]);
  const blueValue = parseInt(rgbValues[2]);
  if (redValue > greenValue && redValue > blueValue) {
    return "Red";
  } else if (greenValue > redValue && greenValue > blueValue) {
    return "Green";
  } else if (blueValue > redValue && blueValue > greenValue) {
    return "Blue";
  }
}

// Updated createTable function
function createTable() {
  "use strict";
  const rows = parseInt(document.querySelector("#size").value);
  const cols = parseInt(document.querySelector("#size").value);
  globalBoardSize = rows;
  matchingTiles = 0;
  selectedCells = 0;
  const tableContainer = document.getElementById("tableHere");
  tableContainer.innerHTML = "";
  const table = createNewTable();
  const difficult = document.getElementById("difficulty");
  let difficulty = difficult.value;
  globalDifficulty = difficulty;
  let cellID = 1;
  for (let i = 0; i < rows; i++) {
    const row = createNewRow();
    for (let j = 0; j < cols; j++) {
      let randomColor = getRandomRGBColor(difficulty);
      const cell = createNewCell(cellID, randomColor);
      cellID++;
      let rgbValues = randomColor.match(/\d+/g); // Define rgbValues here
      let maxComponent = getMaxColorComponent(randomColor);
      cell.innerHTML = `rgb(${rgbValues[0]}, ${rgbValues[1]}, ${rgbValues[2]}) <br> ${maxComponent}`;
      row.appendChild(cell);
    }
    table.appendChild(row);
  }
  tableContainer.appendChild(table);
  tableLoaded();
  updateInfo(document.getElementById("colours"));
}

// Function to check if the table has been clicked
function tableLoaded() {
  // Code for checking table click
  let tableListener = document.getElementById("gameTable");
  tableListener.addEventListener("click", function (event) {
    let eventT = event.target;
    updateInfo(eventT);
  });
}

// Function to update the info bar at the bottom of the screen
function updateInfo(target) {
  // Code for updating info
  let infoText = document.getElementById("noti");
  if (target.tagName.toLowerCase() == "td") {
    target.classList.toggle("highlighted");
  }
  let elementsSelected = document.querySelectorAll(".highlighted");
  let elementCount = elementsSelected.length;
  let guessSelected = elementCount;
  infoText.textContent =
    "Searching for " +
    lookingForColour +
    " tiles! your target is " +
    matchingTiles +
    "! you have selected " +
    elementCount +
    "!";
}

// Function to validate the player's name
function validateName(input) {
  // Code for validating name
  const playerNameError = document.getElementById("playerNameError");
  const startGameButton = document.getElementById("startGameButton");

  if (input.validity.valid) {
    playerNameError.textContent = ""; // Clear error message
    startGameButton.disabled = false;
  } else if (input.validity.valueMissing) {
    playerNameError.textContent = "Value is missing";
    startGameButton.disabled = true;
  } else if (input.validity.patternMismatch) {
    playerNameError.textContent = "Name is invalid";
    startGameButton.disabled = true;
  }
}

// Function to validate the game size
function validateSize(input) {
  // Code for validating size
  const startGameButton = document.getElementById("startGameButton");

  if (input.validity.valid) {
    startGameButton.disabled = false;
  } else if (input.validity.valueMissing) {
    startGameButton.disabled = true;
  } else if (input.validity.rangeUnderflow) {
    startGameButton.disabled = true;
  } else if (input.validity.rangeOverflow) {
    startGameButton.disabled = true;
  }
}

// Function to calculate the player's score
function getScore(numCorrect, numSelected, boardSize, difficulty) {
  // Code for calculating score
  const percent = (2 * numCorrect - numSelected) / (boardSize * boardSize);
  return Math.floor(percent * 100 * boardSize * (difficulty + 1));
}

// Function to get the player's guess
function getPlayerGuess(cellID) {
  let element = document.getElementById(cellID);
  let wasHighlighted = element.classList.contains("highlighted");
  let wasRightColour = element.getAttribute("right-colour") === "true";
  return { wasHighlighted, wasRightColour };
}

// Function to calculate the correct percentage
function calculateCorrectPercentage(numCorrect, boardSize) {
  let correctValue = (numCorrect / (boardSize * boardSize)) * 100;
  return Math.round(correctValue * 10) / 10;
}

// Function to determine the success message
function getSuccessMessage(correctPercent) {
  return correctPercent > 60 ? "Success" : "Failure";
}

// Function to enable game options
function enableGameOptions() {
  document.getElementById("colours").disabled = false;
  document.getElementById("name").disabled = false;
  document.getElementById("size").disabled = false;
  document.getElementById("difficulty").disabled = false;
}

// Updated submitGuess function
function submitGuess() {
  const playerName = document.querySelector("#name").value;
  let rows = parseInt(document.querySelector("#size").value);
  let cols = parseInt(document.querySelector("#size").value);
  let numCorrect = 0;
  let cellID = 1;
  let numSelected = 0;
  for (let i = 0; i < rows; i++) {
    for (let j = 0; j < cols; j++) {
      let { wasHighlighted, wasRightColour } = getPlayerGuess(cellID);
      cellID++;
      if (wasRightColour == wasHighlighted) {
        numCorrect++;
      }
      if (wasHighlighted) {
        numSelected++;
      }
    }
  }
  let correctPercent = calculateCorrectPercentage(numCorrect, globalBoardSize);
  let success = getSuccessMessage(correctPercent);
  alert(
    success +
      "! you completed the board with an accuracy of " +
      correctPercent +
      "%"
  );
  let score = getScore(
    numCorrect,
    numSelected,
    globalBoardSize,
    globalDifficulty
  );
  gameCompleted(playerName, score);
  enableGameOptions();
}

// Function for cheat code
document.addEventListener("keydown", function (event) {
  // Code for cheat code
  if (event.key === "C") {
    const cell = document.querySelectorAll("td");
    cell.forEach((td) => {
      if (td.style.color !== "black") {
        td.dataset.originalColor = getComputedStyle(td).color;
        td.style.color = "black";
      } else {
        td.style.color = td.dataset.originalColor;
      }
    });
  }
});

// Function to get high scores from local storage
function getHighScores() {
  // Code for getting high scores
  const storedScores = localStorage.getItem("highScores");
  return JSON.parse(storedScores) || [];
}

// Function to display high scores
function displayHighScores() {
  // Code for displaying high scores
  const highScoresTable = document.getElementById("highScoresTable");
  highScoresTable.innerHTML = ""; // Clear the table

  const highScores = getHighScores();
  highScores.sort((a, b) => b.score - a.score); // Sort in descending order

  for (let i = 0; i < Math.min(10, highScores.length); i++) {
    const row = highScoresTable.insertRow(i);
    const cell1 = row.insertCell(0);
    const cell2 = row.insertCell(1);

    cell1.textContent = highScores[i].playerName;
    cell2.textContent = highScores[i].score;
  }
}
// Function to add a new high score
function addHighScore(playerName, score) {
  const highScores = getHighScores();
  highScores.push({ playerName, score });
  storeHighScores(highScores);
}
// Function to clear high scores
function clearHighScores() {
  localStorage.removeItem("highScores");
  displayHighScores(); // Refresh the table
}
// Function when a game is completed to add the score to high scores
function gameCompleted(playerName, score) {
  const highScoresTable = document.getElementById("highScoresTable");
  highScoresTable.innerHTML = ""; // Clear the table

  const highScores = getHighScores();
  highScores.sort((a, b) => b.score - a.score); // Sort in descending order

  for (let i = 0; i < Math.min(10, highScores.length); i++) {
    const row = highScoresTable.insertRow(i);
    const cell1 = row.insertCell(0);
    const cell2 = row.insertCell(1);

    cell1.textContent = highScores[i].playerName;
    cell2.textContent = highScores[i].score;
  }
}

// Function to add a new high score
function addHighScore(playerName, score) {
  // Code for adding high score
  const highScores = getHighScores();
  highScores.push({ playerName, score });
  storeHighScores(highScores);
}

// Function to clear high scores
function clearHighScores() {
  // Code for clearing high scores
  localStorage.removeItem("highScores");
  displayHighScores(); // Refresh the table
}

// Function to handle game completion
function gameCompleted(playerName, score) {
  // Code for handling game completion
  addHighScore(playerName, score);
  displayHighScores();
}

// Function to store high scores in local storage
function storeHighScores(scores) {
  // Code for storing high scores
  localStorage.setItem("highScores", JSON.stringify(scores));
}
