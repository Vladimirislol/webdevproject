"use strict";

let lookingForColour;
let colourIndex;
let matchingTiles = 0;
let selectedCells = 0;
let guessSelected = 0;
let globalBoardSize = 0;
let globalDifficulty = 0;

document.addEventListener("DOMContentLoaded", setup);

// Setup function to initialize the game
function setup() {
  // Initialize game buttons and their event listeners
  initializeGameButtons();

  // Display high scores
  displayHighScores();
}
