// Function to initialize game buttons and their event listeners
function initializeGameButtons() {
  const startGameButton = document.getElementById("startGameButton");
  const submitGuessesButton = document.getElementById("submitGuessesButton");
  const clearHighScoresButton = document.getElementById("resetButton");

  // Set background color for start game button
  setBackgroundColour(startGameButton);

  // Event listener for color change
  document.querySelector("#colours").addEventListener("change", function () {
    setBackgroundColour(startGameButton);
  });

  // Event listener for submit guesses button
  submitGuessesButton.addEventListener("click", submitGuess);

  // Event listener for start game button
  startGameButton.addEventListener("click", startGame);

  // Event listener for clear high scores button
  clearHighScoresButton.addEventListener("click", clearHighScores);
}

// Function to set background color for start game button
function setBackgroundColour(startGameButton) {
  let backgroundColour = document.querySelector("#colours").value;
  if (backgroundColour == "blue") {
    backgroundColour = "lightblue";
  }
  startGameButton.style.background = backgroundColour;
}

// Function to start the game
function startGame(event) {
  event.preventDefault();
  lookingForColour = document.querySelector("#colours").value;
  submitGuessesButton.disabled = false;
  setColourIndex();
  createTable();
  disableGameOptions();
}

// Function to set colour index based on the selected colour
function setColourIndex() {
  if (lookingForColour == "red") {
    colourIndex = 0;
  } else if (lookingForColour == "green") {
    colourIndex = 1;
  } else {
    colourIndex = 2;
  }
}

// Function to disable game options after the game starts
function disableGameOptions() {
  document.getElementById("colours").disabled = true;
  document.getElementById("name").disabled = true;
  document.getElementById("size").disabled = true;
  document.getElementById("difficulty").disabled = true;
}
