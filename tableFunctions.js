// Function to create a new table
function createNewTable() {
  const table = document.createElement("table");
  table.id = "gameTable";
  return table;
}

// Function to create a new row
function createNewRow() {
  return document.createElement("tr");
}

// Function to create a new cell
function createNewCell(id, color) {
  const cell = document.createElement("td");
  cell.style.color = color;
  cell.style.backgroundColor = color;
  cell.setAttribute("right-colour", isColour(color));
  cell.id = id;
  return cell;
}

// Function to determine the color component with the highest value
function getMaxColorComponent(color) {
  let rgbValues = color.match(/\d+/g);
  const redValue = parseInt(rgbValues[0]);
  const greenValue = parseInt(rgbValues[1]);
  const blueValue = parseInt(rgbValues[2]);
  if (redValue > greenValue && redValue > blueValue) {
    return "Red";
  } else if (greenValue > redValue && greenValue > blueValue) {
    return "Green";
  } else if (blueValue > redValue && blueValue > greenValue) {
    return "Blue";
  }
}

// Updated createTable function
function createTable() {
  "use strict";
  const rows = parseInt(document.querySelector("#size").value);
  const cols = parseInt(document.querySelector("#size").value);
  globalBoardSize = rows;
  matchingTiles = 0;
  selectedCells = 0;
  const tableContainer = document.getElementById("tableHere");
  tableContainer.innerHTML = "";
  const table = createNewTable();
  const difficult = document.getElementById("difficulty");
  let difficulty = difficult.value;
  globalDifficulty = difficulty;
  let cellID = 1;
  for (let i = 0; i < rows; i++) {
    const row = createNewRow();
    for (let j = 0; j < cols; j++) {
      let randomColor = getRandomRGBColor(difficulty);
      const cell = createNewCell(cellID, randomColor);
      cellID++;
      let rgbValues = randomColor.match(/\d+/g); // Define rgbValues here
      let maxComponent = getMaxColorComponent(randomColor);
      cell.innerHTML = `rgb(${rgbValues[0]}, ${rgbValues[1]}, ${rgbValues[2]}) <br> ${maxComponent}`;
      row.appendChild(cell);
    }
    table.appendChild(row);
  }
  tableContainer.appendChild(table);
  tableLoaded();
  updateInfo(document.getElementById("colours"));
}

// Function to check if the table has been clicked
function tableLoaded() {
  // Code for checking table click
  let tableListener = document.getElementById("gameTable");
  tableListener.addEventListener("click", function (event) {
    let eventT = event.target;
    updateInfo(eventT);
  });
}

// Function to update the info bar at the bottom of the screen
function updateInfo(target) {
  // Code for updating info
  let infoText = document.getElementById("noti");
  if (target.tagName.toLowerCase() == "td") {
    target.classList.toggle("highlighted");
  }
  let elementsSelected = document.querySelectorAll(".highlighted");
  let elementCount = elementsSelected.length;
  let guessSelected = elementCount;
  infoText.textContent =
    "Searching for " +
    lookingForColour +
    " tiles! your target is " +
    matchingTiles +
    "! you have selected " +
    elementCount +
    "!";
}
