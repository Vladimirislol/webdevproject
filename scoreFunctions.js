// Function to get high scores from local storage
function getHighScores() {
  // Code for getting high scores
  const storedScores = localStorage.getItem("highScores");
  return JSON.parse(storedScores) || [];
}

// Function to display high scores
function displayHighScores() {
  // Code for displaying high scores
  const highScoresTable = document.getElementById("highScoresTable");
  highScoresTable.innerHTML = ""; // Clear the table

  const highScores = getHighScores();
  highScores.sort((a, b) => b.score - a.score); // Sort in descending order

  for (let i = 0; i < Math.min(10, highScores.length); i++) {
    const row = highScoresTable.insertRow(i);
    const cell1 = row.insertCell(0);
    const cell2 = row.insertCell(1);

    cell1.textContent = highScores[i].playerName;
    cell2.textContent = highScores[i].score;
  }
}
// Function to add a new high score
function addHighScore(playerName, score) {
  const highScores = getHighScores();
  highScores.push({ playerName, score });
  storeHighScores(highScores);
}
// Function to clear high scores
function clearHighScores() {
  localStorage.removeItem("highScores");
  displayHighScores(); // Refresh the table
}
// Function when a game is completed to add the score to high scores
function gameCompleted(playerName, score) {
  const highScoresTable = document.getElementById("highScoresTable");
  highScoresTable.innerHTML = ""; // Clear the table

  const highScores = getHighScores();
  highScores.sort((a, b) => b.score - a.score); // Sort in descending order

  for (let i = 0; i < Math.min(10, highScores.length); i++) {
    const row = highScoresTable.insertRow(i);
    const cell1 = row.insertCell(0);
    const cell2 = row.insertCell(1);

    cell1.textContent = highScores[i].playerName;
    cell2.textContent = highScores[i].score;
  }
}

// Function to add a new high score
function addHighScore(playerName, score) {
  // Code for adding high score
  const highScores = getHighScores();
  highScores.push({ playerName, score });
  storeHighScores(highScores);
}

// Function to clear high scores
function clearHighScores() {
  // Code for clearing high scores
  localStorage.removeItem("highScores");
  displayHighScores(); // Refresh the table
}

// Function to handle game completion
function gameCompleted(playerName, score) {
  // Code for handling game completion
  addHighScore(playerName, score);
  displayHighScores();
}

// Function to store high scores in local storage
function storeHighScores(scores) {
  // Code for storing high scores
  localStorage.setItem("highScores", JSON.stringify(scores));
}
